FROM node:12 as builder
WORKDIR /workspace
COPY . .
CMD yarn && yarn start
EXPOSE 8080