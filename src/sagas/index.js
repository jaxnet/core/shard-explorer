import {all} from "redux-saga/effects";
import {Sagas} from "../modules";


export default function* rootSaga() {
    yield all([
        Sagas(),
    ]);
}
