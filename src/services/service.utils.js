import {BYTES_IN_MB, DATE_FORMAT} from "./service.constants";
import moment from "moment/moment";

export const parseJwt = token => {
    let base64Url = token.split(".")[0];
    let base64 = base64Url.replace("-", "+").replace("_", "/");
    return JSON.parse(decodeURIComponent(escape(window.atob(base64))));
};

/* Working with React Tables */
export const createColumn = (Header, accessor, rest = {}) => {
    if (accessor === undefined) {
        return {Header, accessor: Header, ...rest};
    }
    return {Header, accessor, ...rest};
};


export const getMbFromBytes = bytes => {
    return Math.round(bytes / BYTES_IN_MB);
};

export const getUnixDate = unixDate =>
    moment.unix(unixDate / 1000000000).format(DATE_FORMAT);

export const isEmpty = value => {
    return (
        value === null ||
        value === "undefined" ||
        value.length === 0 ||
        value === "null"
    );
};

