import axios from "axios";

export const API_URL = process.env.REACT_APP_INDEXER_RPC + "/api/v1";

let requestId = 0
const rpcApi = (url) => ({
    Call: (method, params) => {
        requestId++
        return axios.post(
            url,
            {
                "jsonrpc": "2.0",
                "method": method,
                "params": params,
                "id": requestId,
            }
        )
    }
})

const shardsAPI = api => {
    return {
        List: ({skip, take, sort, direction}) => api.Call("blockchain.Chains", [{
            "skip": skip,
            "take": take,
            "sort": sort,
            "direction": direction
        }]),
        Blocks: ({chain, skip, take, sort, direction}) => api.Call("blockchain.Blocks", [{
            "skip": skip,
            "take": take,
            "chain": chain,
            "sort": sort,
            "direction": direction
        }]),
        Block: ({chain, height}) => api.Call("blockchain.Block", [{"chain": chain, "height": height}]),
        Transaction: ({chain, hash}) => api.Call("blockchain.Transaction", [{"chain": chain, "hash": hash}]),
        Accounts: ({chainId, skip, take}) => api.Call("blockchain.Accounts", [{
            "skip": skip,
            "take": take,
            "chain": chainId
        }]),
        Account: ({address}) => api.Call("blockchain.AddressTransactions", [{"address": address}]),
    };
};

export const RPC = {
    Chains: shardsAPI(rpcApi(API_URL)),
};
