export const DATE_FORMAT = "YYYY-MM-DD HH:mm:ss";
export const BYTES_IN_MB = 1048576;