import {combineReducers} from 'redux';
import { connectRouter } from 'connected-react-router'
import {reducer as formReducer} from 'redux-form';
import IndexerSaga from "./module.indexer";
import SystemSaga from "./module.system";
import {all} from "redux-saga/effects";

const system = SystemSaga;
const indexer = IndexerSaga;

export const Sagas = function* () {
    yield all([...system.sagas, ...indexer.sagas])
}

const rootReducer = (history) => combineReducers({
    router: connectRouter(history),
    form: formReducer,
    indexer: IndexerSaga.Reduce,
    system: system.Reduce,
})

export default rootReducer