import {RPC} from "../services/service.api";
import {Module} from "../utils/saga.module";
import {Map} from "immutable";

export const Indexer = ({
    Chains: {
        List: (params) => ({type: "indexer.chains/LIST", payload: params}),
    },
    Blocks: {
        List: (params) => ({type: "indexer.blocks/LIST", payload: params}),
        Get: (params) => ({type: "indexer.block/GET", payload: params}),
        GetTransaction: (params) => ({type: "indexer.transaction/GET", payload: params}),
    },
    Accounts: {
        List: (params) => ({type: "indexer.accounts/LIST", payload: params}),
        Get: (params) => ({type: "indexer.accounts/GET", payload: params}),
    },
    Init: () => ({type: "jax.indexer/INIT"}),
});

const arrayToMap = (data) => {
    if (data == null) {
        return Map({
            items: [],
            total: 0,
        })
    }

    return Map(data)
};

const reduce = {
    Chains: {
        List: m => {
            return m.Request(Indexer.Chains.List(), RPC.Chains.List,
                (state, {payload}) => state.mergeIn(['chains', 'list'], arrayToMap(payload.result))
            )
        },
    },
    Blocks: {
        List: m => {
            return m.Request(Indexer.Blocks.List(), RPC.Chains.Blocks,
                (state, {payload}) => state.mergeIn(['blocks', 'list'], arrayToMap(payload.result))
            )
        },
        Get: m => {
            return m.Request(Indexer.Blocks.Get(), RPC.Chains.Block,
                (state, {payload}) => {
                    return state.mergeIn(['blocks', 'current'], Map(payload.result))
                }
            )
        },
        GetTransaction: m => {
            return m.Request(Indexer.Blocks.GetTransaction(), RPC.Chains.Transaction,
                (state, {payload}) => state.mergeIn(['blocks', 'transaction'], arrayToMap(payload.result))
            )
        },
    },
    Accounts: {
        List: m => {
            return m.Request(Indexer.Accounts.List(), RPC.Chains.Accounts,
                (state, {payload}) => state.mergeIn(['accounts', 'list'], arrayToMap(payload.result))
            )
        },
        Get: m => {
            return m.Request(Indexer.Accounts.Get(), RPC.Chains.Account,
                (state, {payload}) => {
                    return state.mergeIn(['accounts', 'current'], Map(payload.result))
                }
            )
        },
    },
};

const listModel = {items: [], total: 0, skip: 0};

export default Module("indexer")
    .Module("chains", {list: listModel, filter: {}}, [
        reduce.Chains.List,
    ])
    .Module("blocks", {list: listModel, current: {}, transaction: {}, filter: {}}, [
        reduce.Blocks.List,
        reduce.Blocks.Get,
        reduce.Blocks.GetTransaction,
    ])
    .Module("accounts", {list: listModel, current: {}, filter: {}}, [
        reduce.Accounts.List,
        reduce.Accounts.Get,
    ])
