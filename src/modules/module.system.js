import {Map} from "immutable";
import {Module} from "../utils/saga.module";
import {initialize as FormActions} from "redux-form";
import {put} from "redux-saga/effects";

export const System = ({
    UI: {
        Lock: (name) => ({type: "system.ui/LOCK", payload: name}),
        Unlock: (name) => ({type: "system.ui/UNLOCK", payload: name}),
        Left: {
            Show: () => ({type: "system.left/SHOW", payload: {}}),
            Close: () => ({type: "system.left/CLOSE", payload: {}}),
        },
        Right: {
            Show: () => ({type: "system.right/SHOW", payload: {}}),
            Close: () => ({type: "system.right/CLOSE", payload: {}}),
        },
    },
    Modal: {
        Open: (data) => ({type: "system.modal/OPEN", payload: data}),
        Close: () => ({type: "system.modal/CLOSE"}),
        CloseAll: () => ({type: "system.modal/CLOSE:all"}),
    }
});


function* openDialog({payload}) {
    yield put(FormActions('dialog', payload.values, false));
    return payload
}

const reduce = {
    UI: {
        Lock: m => m.Action(System.UI.Lock(), (state, {payload}) =>
            state.setIn(["ui", 'lock', payload], true)
        ),
        Unlock: m => m.Action(System.UI.Unlock(), (state, {payload}) =>
            state.deleteIn(["ui", 'lock', payload])
        ),
        Left:
            {
                Show: m => m.Action(System.UI.Left.Show(), (state) =>
                    state.mergeIn(["ui", 'left'], Map({show: true}))
                        .mergeIn(["ui", 'right'], Map({show: false}))
                ),
                Close: m => m.Action(System.UI.Left.Close(), (state) =>
                    state.mergeIn(["ui", 'left'], Map({show: false}))
                )
            }
        ,
        Right: {
            Show: m => m.Action(System.UI.Right.Show(), (state) =>
                state.mergeIn(["ui", 'right'], Map({show: true}))
                    .mergeIn(["ui", 'left'], Map({show: false}))
            ),
            Close:
                m => m.Action(System.UI.Right.Close(), (state) =>
                    state.mergeIn(["ui", 'right'], Map({show: false}))
                )
        }
    },
    Modal: {
        Open: m => m.Action(System.Modal.Open(), (state, {payload}) => {
                state = state.mergeIn(["modal"], Map({
                    show: true,
                    header: payload.header,
                    form: payload.form,
                    actions: payload.actions
                }));
                return state
            },
            openDialog
        ),
        Close:
            m => m.Action(System.Modal.Close(), (state) =>
                state.mergeIn(["modal"], Map({show: false}))
            ),
        CloseAll:
            m => m.Action(System.Modal.CloseAll(), (state) =>
                state.mergeIn(["modal"], Map({show: false}))
                    .mergeIn(["ui", 'left'], Map({show: false}))
                    .mergeIn(["ui", 'right'], Map({show: false}))
            )

    }
};


export default Module("settings")
    .Module("ui", {lock: {}, left: {show: false}, right: {show: false}}, [
        reduce.UI.Lock,
        reduce.UI.Unlock,
        reduce.UI.Left.Show,
        reduce.UI.Left.Close,
        reduce.UI.Right.Show,
        reduce.UI.Right.Close,
    ])
    .Module("modal", {show: false, header: {}, form: [], actions: []}, [
        reduce.Modal.Open,
        reduce.Modal.Close,
        reduce.Modal.CloseAll,
    ])