import {fromJS, Map} from "immutable";
import {CrudAPI} from "../services/service.api";
import {all, call, put, takeLatest} from 'redux-saga/effects'


const getTopic = (module, name) => {
    const topicType = (t) => ({
        request: () => t,
        success: () => t + ":success",
        error: () => t + ":error"
    });
    let path = module + "." + name;
    return {
        list: () => topicType(path + "/LIST"),
        get: () => topicType(path + "/GET"),
        create: () => topicType(path + "/CREATE"),
        update: () => topicType(path + "/UPDATE"),
        delete: () => topicType(path + "/DELETE"),
    }
};

export const CrudActions = (module, name) => {
    let topic = getTopic(module, name);
    let actions = {
        name: name,
        List: (filter, base) => ({type: topic.list().request(), payload: {filter, base}}),
        Get: (id, base) => ({type: topic.get().request(), payload: {id, base}}),
        Create: (item, base) => ({type: topic.create().request(), payload: item}),
        Update: (id, item, base) => ({type: topic.update().request(), payload: {id: id, value: item}}),
        Delete: (id, base) => ({type: topic.delete().request(), payload: {id, base}})
    };
    return actions
};


let crudReducer = (state, action, module, name) => {
    let topic = getTopic(module, name);
    switch (action.type) {
        case topic.list().request():
            state = state.mergeIn([name], Map({
                filter: action.payload,
                updating: true,
            }));
            return state;
        case topic.list().success():
            state = state.mergeIn([name], Map({
                list: action.payload.items,
                total: action.payload.total,
                updating: false,
            }));
            return state;
        case topic.delete().success():
            let list = state.get(name).get("list").filter(x => x.ID !== action.payload);
            return state.setIn([name, "list"], list);
        default:
            return state;
    }
};

export const ListToMap = (list) => {
    return list.items.reduce((acc, item) => {
        return acc.set(item.ID, fromJS(item));
    }, Map());
};

export const ArrayToMap = (list) => {
    let i = 0;
    return list.reduce((acc, item) =>
            acc.set(i++, fromJS(item))
        , Map());
};

export const CrudSaga = (module, name) => {
    let topic = getTopic(module, name);

    let url = module + "/" + name;
    let list = function* ({payload}) {
        try {
            if (payload.base) {
                url = module + payload.base + "/" + name;
            }
            let data = yield call(CrudAPI(url).List, payload.filter);
            let list = data.data;
            yield put({
                type: topic.list().success(),
                payload: list,
            });
        } catch (e) {
            yield put({
                type: topic.list().error(),
            })
        }
    };

    let get = function* ({payload}) {
        try {
            let data = yield call(CrudAPI(url).Get, payload.id);
            yield put({
                type: topic.get().success(),
                payload: data
            });
        } catch (e) {
            console.log('Error:', e);
            yield put({
                type: topic.get().error(),
            })
        }
    };

    let create = function* ({payload}) {
        try {
            let data = yield call(CrudAPI(url).Create, payload.id);
            yield put({
                type: topic.create().success(),
                payload: data
            });
        } catch (e) {
            console.log('Error:', e);
            yield put({
                type: topic.create().error(),
            })
        }
    };

    let update = function* ({payload}) {
        try {
            let data = yield call(CrudAPI(url).Update, payload.id, payload);
            yield put({
                type: topic.update().success(),
                payload: data
            });
        } catch (e) {
            console.log('Error:', e);
            yield put({
                type: topic.update().error(),
            })
        }
    };

    let del = function* ({payload}) {
        try {
            let data = yield call(CrudAPI(url).Delete, payload.id);
            yield put({
                type: topic.delete().success(),
                payload: payload.id
            });
        } catch (e) {
            console.log('Error:', e);
            yield put({
                type: topic.delete().error(),
            })
        }
    };

    return function* () {
        yield all([
            takeLatest(topic.get().request(), get),
            takeLatest(topic.list().request(), list),
            takeLatest(topic.create().request(), create),
            takeLatest(topic.update().request(), update),
            takeLatest(topic.delete().request(), del)
        ]);
    }
};

export const CRUD = (module, name, state) => {
    state[name] = {
        updating: false,
        list: [],
        total: 0,
        filter: {skip: 0, take: 5, page: 0},
    };
    return {Actions: CrudActions(module, name), Reduce: (state, action) => crudReducer(state, action, module, name)}
};




