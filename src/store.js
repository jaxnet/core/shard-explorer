import {createStore, applyMiddleware, compose} from 'redux';
import {routerMiddleware} from 'connected-react-router'
import {createBrowserHistory} from 'history'
import createRootReducer from './modules'
import Sagas from './sagas'
import {bindShortcuts} from 'redux-shortcuts';
import createSagaMiddleware from 'redux-saga'
import {System} from "./modules/module.system";

const sagaMiddleware = createSagaMiddleware();

export const history = createBrowserHistory();

let initialState = {}

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const store = createStore(createRootReducer(history), initialState, composeEnhancer(applyMiddleware(
    routerMiddleware(history), sagaMiddleware,
)));

sagaMiddleware.run(Sagas);

bindShortcuts(
    [['esc'], System.Modal.CloseAll],
)(store.dispatch);

export default store;
