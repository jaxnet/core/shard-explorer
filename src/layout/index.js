import React from 'react';
import './index.scss';
import Navigation from '../app/components/navigation'
import ModalDialog from "../app/components/modal";
import Loader from "../app/components/loader";

export class InnerLayout extends React.Component {
    constructor() {
        super();
        this.state = {
            sidebarStatus: false
        }
    }

    render() {
        return (
            <React.Fragment>
                <ModalDialog/>
                <Loader/>
                <Navigation/>
                {this.props.children}
            </React.Fragment>
        );
    }
}