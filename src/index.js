import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import store, { history } from './store'

import 'bootstrap/dist/js/bootstrap.min';

//Styles
import 'sanitize.css/sanitize.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import './static/style/index.scss'
import App from "./app";

import * as serviceWorker from './registerServiceWorker'

let viewportUnitsBluegill = require('viewport-units-buggyfill');
viewportUnitsBluegill.refresh();


const target = document.querySelector('#root');

render(
    <Provider store={store}>
        <App history={history}/>
    </Provider>,
    target
);

serviceWorker.unregister();


