import React, {Fragment, Component} from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {Route, Switch} from "react-router";
import {ConnectedRouter} from 'connected-react-router'
import Dashboard from "./dashboard";
import NotFound from "./error/404";
// import {Glink} from "../modules/module.manager";
import {ExplorerComponent} from "./explorer";
import {ChainComponent} from "./explorer/chain";
import {ChainBlockComponent} from "./explorer/block/block.details";
import {TransactionComponent} from "./explorer/block/transaction.details";
import {AccountsComponent} from "./explorer/accounts";
import {AccountDetailsComponent} from "./explorer/accounts/account.details";


class App extends Component {
    componentDidMount() {
        // this.props.start();
        // this.props.init();
        // this.props.initWS();
    }

    renderRoutes() {
        // if (this.props.account.loading) {
        //     return App.loadingRoutes();
        // }
        return this.defaultRoutes();
    }

    // static loadingRoutes() {
    //     return (
    //         <ConnectedRouter history={history}>
    //             <Switch>
    //                 <Route exact path="/" component={Loading}/>
    //             </Switch>
    //         </ConnectedRouter>
    //     );
    // }
    //
    // accountRoutes(account) {
    //     if (!account || !account.isAuthorized) {
    //         return <Redirect to="/login"/>;
    //     }
    // }

    defaultRoutes() {
        let {history} = this.props;
        return <Fragment>
            <ConnectedRouter history={history}>
                <Switch>
                    <Route exact path="/" component={Dashboard}/>
                    <Route exact path="/explorer" component={ExplorerComponent}/>
                    <Route exact path="/explorer/account" component={AccountsComponent}/>
                    <Route exact path="/explorer/address/:address" component={AccountDetailsComponent}/>
                    <Route exact path="/explorer/chain/:id" component={ChainComponent}/>
                    <Route exact path="/explorer/chain/:id/block/:block" component={ChainBlockComponent}/>
                    <Route exact path="/explorer/chain/:id/transaction/:hash" component={TransactionComponent}/>
                    <Route component={NotFound}/>
                </Switch>
            </ConnectedRouter>
        </Fragment>
    }

    render() {
        return <Fragment>{this.renderRoutes()}</Fragment>;
    }
}

const mapStateToProps = () => ({});
const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            // init: Glink.Init,
        },
        dispatch
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App);
