import {InnerLayout} from "../../layout";
import React from "react";
import {ExplorerMenu} from "./index.menu";

export const ExplorerLayout = ({children}) => (
    <InnerLayout>
        <div className="side-menu-area">
            <ExplorerMenu/>
            <div className="main-area">
                <div className="container-fluid">
                    {children}
                </div>
            </div>
        </div>
    </InnerLayout>
);