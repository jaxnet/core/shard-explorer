import {NavLink} from "react-router-dom";
import {MdApps as BoardIcon, MdLayers as LayersIcon, MdAccountBalanceWallet as WalletIcon} from "react-icons/md";
import {MdSlowMotionVideo as AssignmentIcon} from "react-icons/md"

import React from "react";
import {bindActionCreators, compose} from "redux";
import connect from "react-redux/es/connect/connect";
import {Indexer} from "../../modules/module.indexer";

class ExplorerMenuComponent extends React.Component {
    componentDidMount = () => {
        this.props.loadList()
    };

    render() {
        return (
            <ul className="menu-bar">
                <NavLink exact to="/explorer" className="menu-item">
                    <BoardIcon className="icon"/>
                    <span className="text">Chains</span>
                </NavLink>
                <NavLink exact to="/explorer/account" className="menu-item">
                    <WalletIcon className="icon"/>
                    <span className="text">Accounts</span>
                </NavLink>
            </ul>
        )
    }
}

const mapStateToProps = state => ({
    loadList: Indexer.Chains.List,
});
const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export const ExplorerMenu = compose(
    connect(mapStateToProps, mapDispatchToProps)
)(ExplorerMenuComponent);
