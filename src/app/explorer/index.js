import React from 'react';
import {bindActionCreators, compose} from 'redux';
import {connect} from 'react-redux';
import './index.scss'
import {ExplorerLayout} from "./_layout";
import {Indexer} from "../../modules/module.indexer";
import {NavLink} from "react-router-dom";
import {Pager} from "../components/pager";

const itemsPerPage = 20

class Explorer extends React.Component {
    componentDidMount = () => {
        this.loadPage(0, itemsPerPage)
    };

    loadPage = (skip, take, sortBy, direction) => {
        this.props.load({skip: skip, take: take, sort: sortBy, direction: direction})
    }

    sortMark = (field, sortBy, direction) =>{
        return field !== sortBy ? '' : direction === 'asc' ? '↑':'↓'
    }

    render() {
        let {list} = this.props
        return (
            <ExplorerLayout>
                {this.props.children}
                <div className='row'>
                    <div className='col-sm'>
                        <div className='card'>
                            <div className='card-body'>
                                <h4 className='card-title'>Chain Explorer</h4>
                                <h6 className='card-subtitle'>List chains</h6>
                                <div className="table-responsive mt-5">
                                    <table className="table table-hover v-middle">
                                        <thead>
                                        <tr>
                                            <th onClick={() => this.loadPage(list.skip, list.take, "shard_id", list.direction === "asc" ? "desc" : "asc")}>ID {this.sortMark("shard_id", list.sort, list.direction)}</th>
                                            {/*<th onClick={() => this.loadPage(list.skip, list.take, "height", list.direction === "asc" ? "desc" : "asc")}>Height {this.sortMark("height", list.sort, list.direction)}</th>*/}
                                            {/*<th>Uniq Addresses</th>*/}
                                            {/*<th>Hash</th>*/}
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {list && list.items && list.items.map((v, i) => (
                                            <tr key={i}>
                                                <td>

                                                    <NavLink to={"/explorer/chain/" + v.Id} className="nav-item">
                                                        Chain {v.Id}
                                                    </NavLink>
                                                </td>
                                            </tr>
                                        ))}
                                        </tbody>
                                    </table>

                                    {list && Pager(list.skip, itemsPerPage, list.total, (skip, take) => {
                                        this.loadPage(skip, take, list.sort, list.direction)
                                    })}
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </ExplorerLayout>
        );
    }
}

const mapStateToProps = ({indexer}) => ({
    list: indexer.getIn(['chains', 'list']).toJS(),
});

const mapDispatchToProps = dispatch => bindActionCreators({
    load: Indexer.Chains.List,
}, dispatch);

export const ExplorerComponent = compose(
    connect(mapStateToProps, mapDispatchToProps)
)(Explorer);
