import React from 'react';
import {bindActionCreators, compose} from 'redux';
import {connect} from 'react-redux';
import '../index.scss'
import {ExplorerLayout} from "../_layout";
import {NavLink, withRouter, useParams} from "react-router-dom";
import {Indexer} from "../../../modules/module.indexer";

class Accounts extends React.Component {
    componentDidMount = () => {
        this.props.loadList({
            chain: Number(this.props.match.params.id),
            skip: 0,
            take: 100,
        })
    };

    render() {
        let {list} = this.props
        let id = this.props.match.params.id
        let chainName = ""

        if (id === "0") {
            chainName = "Beacon"
        } else {
            chainName = "Shard " + id
        }

        return (
            <ExplorerLayout>
                {this.props.children}
                <div className='row'>
                    <div className='col-sm'>
                        <div className='card'>
                            <div className='card-body'>
                                <h4 className='card-title'>Accounts List</h4>
                                <h6 className='card-subtitle'></h6>
                                <div className="table-responsive mt-5">
                                    <table className="table table-hover v-middle">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Balance</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {list && list.items && list.items.map((v, i) => (
                                            <tr key={i}>
                                                <td>
                                                    <NavLink
                                                        to={"/explorer/address/" + v.Address}
                                                        className="nav-item">
                                                        {v.Address}
                                                    </NavLink>

                                                </td>
                                                <td>
                                                    {v.Balance}
                                                </td>
                                            </tr>
                                        ))}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </ExplorerLayout>
        );
    }
}

const mapStateToProps = ({indexer}) => ({
    list: indexer.getIn(['accounts', 'list']).toJS(),
});

const mapDispatchToProps = dispatch => bindActionCreators({
    loadList: Indexer.Accounts.List,
}, dispatch);

export const AccountsComponent = compose(
    withRouter,
    connect(mapStateToProps, mapDispatchToProps)
)(Accounts);
