import React from 'react';
import {bindActionCreators, compose} from 'redux';
import {connect} from 'react-redux';
import '../index.scss'
import {ExplorerLayout} from "../_layout";
import {NavLink} from "react-router-dom";
import {Indexer} from "../../../modules/module.indexer";

class AccountDetails extends React.Component {
    componentDidMount = () => {
        this.loadPage(this.props.match.params.address)
    };

    loadPage = (address) => {
        console.log(address)
        this.props.load({address})
    }

    formatTimestamp = (timestamp) => {
        const dateObject = new Date(timestamp / 1000000)
        return dateObject.toLocaleString() //2019-12-9 10:30:15
    }

    render() {
        let {account} = this.props
        let address = this.props.match.params.address
        return (
            <ExplorerLayout>
                {this.props.children}
                <React.Fragment>

                    <div className='row'>
                        <div className='col-sm'>
                            <div className='card'>
                                <div className="card-header">
                                    <h4 className='card-title'>Address: {address}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-sm'>
                            <div className="card">
                                <div className="card-header">
                                    Balance Change
                                </div>
                                <div className="card-body">
                                    <div className="table-responsive mt-5">
                                        <table className="table table-hover v-middle">
                                            <thead>
                                            <tr>
                                                <th>From Tx</th>
                                                <th>To Tx</th>
                                                <th>Balance Change</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {account && account.List && account.List.map((v, i) => (
                                                <tr key={i}>
                                                    <td>
                                                        {v.tx_from == '0000000000000000000000000000000000000000000000000000000000000000' ? ('coinbase'):(
                                                            <NavLink
                                                                to={"/explorer/chain/" + v.shard_id + "/transaction/" + v.tx_from}
                                                                className="nav-item">
                                                                {v.tx_from}
                                                            </NavLink>
                                                        )}
                                                    </td>
                                                    <td>
                                                        <NavLink
                                                            to={"/explorer/chain/" + v.shard_id + "/transaction/" + v.tx_to}
                                                            className="nav-item">
                                                            {v.tx_to}
                                                        </NavLink>
                                                    </td>
                                                    <td>
                                                        {v.value}
                                                    </td>
                                                </tr>
                                            ))}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </React.Fragment>
            </ExplorerLayout>
        );
    }
}

const mapStateToProps = ({indexer}) => ({
    account: indexer.getIn(['accounts', 'current']).toJS(),
});

const mapDispatchToProps = dispatch => bindActionCreators({
    load: Indexer.Accounts.Get,
}, dispatch);

export const AccountDetailsComponent = compose(
    connect(mapStateToProps, mapDispatchToProps)
)(AccountDetails);
