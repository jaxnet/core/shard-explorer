import React from 'react';
import {bindActionCreators, compose} from 'redux';
import {connect} from 'react-redux';
import '../index.scss'
import {ExplorerLayout} from "../_layout";
import {NavLink} from "react-router-dom";
import {Indexer} from "../../../modules/module.indexer";

class ChainBlock extends React.Component {
    componentDidMount = () => {
        this.loadPage(Number(this.props.match.params.id),
            Number(this.props.match.params.block))
    };

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.block && Number(prevProps.block.height) !== Number(this.props.match.params.block)) {
            this.loadPage(Number(this.props.match.params.id),
                Number(this.props.match.params.block))
        }
    }

    loadPage = (chain, height) => {
        this.props.load({chain, height})
    }

    formatTimestamp = (timestamp) => {
        const dateObject = new Date(timestamp / 1000000)
        return dateObject.toLocaleString() //2019-12-9 10:30:15
    }

    render() {
        let shardId = Number(this.props.match.params.id)
        let blockId = Number(this.props.match.params.block)

        let prevBlock = Math.max(0, blockId - 1)
        let nextBlock = Math.max(0, blockId + 1)
        let {block} = this.props

        return (
            <ExplorerLayout>
                {this.props.children}
                <React.Fragment>

                    <div className='row'>
                        <div className='col-sm'>
                            <div className='card'>
                                <div className="card-header">
                                    <h4 className='card-title'>Shard Info</h4>
                                </div>
                                <ul className="list-group list-group-flush">
                                    <li className="list-group-item">ID: {shardId}</li>
                                </ul>
                                <ul className="list-group list-group-flush">
                                    <li className="list-group-item">Total Blocks: {block.total_blocks}</li>
                                </ul>

                            </div>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-sm'>
                            <div className="card">
                                <div className="card-header">
                                    Block Header

                                    <ul className="navbar-nav">
                                        <NavLink to={"/explorer/chain/" + shardId + "/block/" + prevBlock}
                                                 className="nav-item">Back</NavLink>
                                        <NavLink to={"/explorer/chain/" + shardId + "/block/" + nextBlock}
                                                 className="nav-item">Forward</NavLink>
                                    </ul>
                                </div>
                                <ul className="list-group list-group-flush">
                                    <li className="list-group-item">Hash: {block.hash}</li>
                                    <li className="list-group-item">Date: {this.formatTimestamp(block.timestamp)}</li>
                                    <li className="list-group-item">Height: {block.height}</li>
                                    <li className="list-group-item">Bits: {block.bits}</li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div className='row'>
                        <div className='col-sm'>
                            <div className="card">
                                <div className="card-header">
                                    Transactions
                                </div>
                                <div className="card-body">
                                    <div className="table-responsive mt-5">
                                        <table className="table table-hover v-middle">
                                            <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Transaction ID</th>
                                                <th>Inputs</th>
                                                <th>Outputs</th>
                                                <th>Amount</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {block && block.transactions && block.transactions.map((v, i) => (
                                                <tr key={i}>
                                                    <td>
                                                        {this.formatTimestamp(v.lock_time)}
                                                    </td>
                                                    <td>
                                                        <NavLink
                                                            to={"/explorer/chain/" + shardId + "/transaction/" + v.hash}
                                                            className="nav-item">
                                                            {v.hash}
                                                        </NavLink>

                                                    </td>
                                                    <td>
                                                        {v.tx_in && v.tx_in.length}
                                                    </td>
                                                    <td>
                                                        {v.tx_out && v.tx_out.length}
                                                    </td>
                                                    <td>
                                                        {v.tx_out.reduce((res, y) => {
                                                            res.Value = y.Value
                                                            return res
                                                        }).Value}
                                                    </td>
                                                </tr>
                                            ))}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </React.Fragment>
            </ExplorerLayout>
        );
    }
}

const mapStateToProps = ({indexer}) => ({
    block: indexer.getIn(['blocks', 'current']).toJS(),
});

const mapDispatchToProps = dispatch => bindActionCreators({
    load: Indexer.Blocks.Get,
}, dispatch);

export const ChainBlockComponent = compose(
    connect(mapStateToProps, mapDispatchToProps)
)(ChainBlock);
