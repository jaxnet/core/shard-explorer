import React from 'react';
import {bindActionCreators, compose} from 'redux';
import {connect} from 'react-redux';
import '../index.scss'
import {ExplorerLayout} from "../_layout";
import {NavLink} from "react-router-dom";
import {Indexer} from "../../../modules/module.indexer";

class Transaction extends React.Component {
    componentDidMount = () => {
        this.loadPage(Number(this.props.match.params.id), this.props.match.params.hash)
    };

    componentDidUpdate(prevProps, prevState, snapshot) {

        if (prevProps.transaction && prevProps.transaction.hash && prevProps.transaction.hash !== this.props.match.params.hash) {
            this.loadPage(Number(this.props.match.params.id), this.props.match.params.hash)
            //     console.log(prevProps.transaction.hash)
            //     // this.loadPage(Number(this.props.match.params.id), this.props.match.params.hash)
        }
    }

    loadPage = (chain, hash) => {
        this.props.load({chain, hash})
    }

    formatTimestamp = (timestamp) => {
        const dateObject = new Date(timestamp * 1000)
        return dateObject.toLocaleString()
    }

    hexToBase64 = (hexstring) => {
        console.log("", hexstring)
        if (hexstring != null && hexstring != undefined) {
            return btoa(hexstring.match(/\w{2}/g).map(function (a) {
                return String.fromCharCode(parseInt(a, 16));
            }).join(""));
        } else {
            return ''
        }
    }

    render() {
        let shardId = Number(this.props.match.params.id)

        let {transaction} = this.props

        let trx = transaction
        return trx ? (
            <ExplorerLayout>
                {this.props.children}
                <React.Fragment>
                    <div className='row'>
                        <div className='col-sm'>
                            <div className="card">
                                <div className="card-header">
                                    Transaction <b>{this.props.match.params.hash}</b>
                                </div>
                                <ul className="list-group list-group-flush">
                                    <li className="list-group-item">Shard: {shardId}</li>
                                    <li className="list-group-item">Time: {this.formatTimestamp(trx.time)}</li>
                                    <li className="list-group-item">Block:
                                        <NavLink
                                            to={"/explorer/chain/" + shardId + "/block/" + transaction.height}
                                            className="nav-item">
                                            {transaction.height} - {trx.blockhash}
                                        </NavLink>
                                    </li>
                                    <li className="list-group-item">size: {trx.size}</li>
                                    <li className="list-group-item">Confirmations: {trx.confirmations}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-sm'>
                            <div className="card">
                                <div className="card-header">
                                    Inputs
                                </div>
                                <div className="card-body">
                                    <div className="table-responsive mt-5">
                                        <table className="table table-hover v-middle">
                                            <thead>
                                            <tr>
                                                <th>Shard</th>
                                                <th>Index</th>
                                                <th>Prev Tx</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {trx && trx.vin && trx.vin.map((v, i) => (
                                                <tr key={i}>
                                                    <td>
                                                        {v.shard}
                                                    </td>
                                                    <td>
                                                        {v.txid != "" && (v.vout)}
                                                    </td>
                                                    <td>
                                                        {v.txid == "" ? ("coinbase") : (
                                                            <NavLink
                                                                to={"/explorer/chain/" + v.shard + "/transaction/" + v.txid}
                                                                className="nav-item">
                                                                {v.txid}
                                                            </NavLink>
                                                        )}
                                                    </td>
                                                </tr>
                                            ))}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div className='col-sm'>
                            <div className="card">
                                <div className="card-header">
                                    Outputs
                                </div>
                                <div className="card-body">
                                    <div className="table-responsive mt-5">
                                        <table className="table table-hover v-middle">
                                            <thead>
                                            <tr>
                                                <th>Index</th>
                                                <th>Address</th>
                                                <th>Script</th>
                                                <th>Value</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {trx && trx.vout && trx.vout.map((v, i) => (
                                                <tr key={i}>
                                                    <td>
                                                        {v.n}
                                                    </td>
                                                    <td>
                                                        {v.scriptPubKey.addresses.map((addr, j) => (
                                                            <NavLink key={j}
                                                                     to={"/explorer/address/" + addr}
                                                                     className="nav-item">
                                                                {addr}
                                                            </NavLink>
                                                        ))}
                                                    </td>
                                                    <td>{v.scriptPubKey.type}</td>
                                                    <td>{v.value}</td>
                                                </tr>
                                            ))}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </React.Fragment>
            </ExplorerLayout>
        ) : (
            <ExplorerLayout>
                {this.props.children}
                <React.Fragment></React.Fragment>
            </ExplorerLayout>
        );
    }
}

const mapStateToProps = ({indexer}) => ({
    transaction: indexer.getIn(['blocks', 'transaction']).toJS(),
});

const mapDispatchToProps = dispatch => bindActionCreators({
    load: Indexer.Blocks.GetTransaction,
}, dispatch);

export const TransactionComponent = compose(
    connect(mapStateToProps, mapDispatchToProps)
)(Transaction);
