import React from 'react';
import {bindActionCreators, compose} from 'redux';
import {connect} from 'react-redux';
import '../index.scss'
import {ExplorerLayout} from "../_layout";
import {NavLink, withRouter, useParams} from "react-router-dom";
import {Indexer} from "../../../modules/module.indexer";
import {Pager} from "../../components/pager";

const itemsPerPage = 20

class ChainBlocks extends React.Component {

    componentDidMount = () => {
        this.changePage(0, itemsPerPage, 'height', 'desc')
    };

    changePage = (skip, take, sortBy, direction) => {
        this.props.load({chain: Number(this.props.match.params.id), skip: skip, take: take, sort: sortBy, direction: direction})
    }

    sortMark = (field, sortBy, direction) =>{
        return field !== sortBy ? '' : direction === 'asc' ? '↑':'↓'
    }

    render() {
        let {list} = this.props
        let id = this.props.match.params.id
        let chainName = ""
        console.log("chain ", id)

        if (id === "0"){
            chainName = "Beacon"
        } else{
            chainName = "Shard " + id
        }

        return (
            <ExplorerLayout>
                {this.props.children}
                <div className='row'>
                    <div className='col-sm'>
                        <div className='card'>
                            <div className='card-body'>
                                <h4 className='card-title'>Chain: {chainName}</h4>
                                <h6 className='card-subtitle'>List Blocks</h6>
                                <div className="table-responsive mt-5">
                                    <table className="table table-hover v-middle">
                                        <thead>
                                        <tr>
                                            <th onClick={() => this.changePage(list.skip, list.take, "height", list.direction === "asc" ? "desc" : "asc")}>Height {this.sortMark("height", list.sort, list.direction)}</th>
                                            <th>Hash</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {list && list.items && list.items.map((v, i) => (
                                            <tr key={i}>
                                                <td>
                                                    {v.Height}
                                                </td>
                                                <td>
                                                    <NavLink to={"/explorer/chain/" + id +"/block/" + v.Height} className="nav-item">
                                                        {v.Hash}
                                                    </NavLink>
                                                </td>
                                            </tr>
                                        ))}
                                        </tbody>
                                    </table>
                                    {list && Pager(list.skip, itemsPerPage, list.total, (skip, take) => {
                                        this.changePage(skip, take, list.sort, list.direction)
                                    })}
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </ExplorerLayout>
        );
    }
}

const mapStateToProps = ({indexer}) => ({
    list: indexer.getIn(['blocks', 'list']).toJS(),
});

const mapDispatchToProps = dispatch => bindActionCreators({
    load: Indexer.Blocks.List,
}, dispatch);

export const ChainComponent = compose(
    withRouter,
    connect(mapStateToProps, mapDispatchToProps)
)(ChainBlocks);
