import React from 'react';
import {bindActionCreators, compose} from 'redux';
import {connect} from 'react-redux';
import {
  withRouter,
} from 'react-router-dom';
import {InnerLayout} from "../../../layout/index";
import './index.scss';

const NotFound = props => (
  <InnerLayout>
    <span className="notfound">404</span>
  </InnerLayout>
);

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps)
)(NotFound);