import React from 'react';
import {
  VictoryChart,
  VictoryAxis,
  VictoryLabel,
  VictoryBar,
  VictoryVoronoiContainer,
  Flyout,
  VictoryTooltip
} from 'victory';
import './Barchart.css';
import moment from "moment/moment";

export default (props) => (
  <div className="bar-chart">
    <VictoryChart
      width={2000}
      height={600}
      domainPadding={[15,15]}
      domain={
        {
          y:[props.momentLow - (props.momentHigh - props.momentLow)*0.1, props.momentHigh + (props.momentHigh - props.momentLow)*0.1]
        }
      }
      containerComponent={
        <VictoryVoronoiContainer
          voronoiDimension="x"
        />
      }
    >
      <VictoryAxis
        orientation="left"
        style={{
          axis: {display: "none"},
          grid: {
            stroke: "#384150"
          }
        }}
        tickLabelComponent={<VictoryLabel style={{fill:"#ffffff", fontSize: 22}}/>}
      />
      <VictoryBar
        data={props.momentData}
        style={{
          data: {fill: "#586477", width: 30}
        }}
        barRatio={0.1}
        x="time"
        y="price"
        alignment="middle"
        labels={(d) =>
          `Time: ${moment(parseInt(d.time, 10)).format('YYYY MMMM Do, H:mm:ss')}
                          Price: ${d.price}`
        }
        labelComponent={<VictoryTooltip
          style={{fontSize: 22}}
          flyoutComponent={
            <Flyout
              width={320}
            />
          }
          flyoutStyle={{
            fill: 'rgba(5, 188, 254, 0.75)'
          }}
        />}
      />
    </VictoryChart>
  </div>
)