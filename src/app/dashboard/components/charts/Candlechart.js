import React from 'react';
import moment from "moment/moment";
import {
  VictoryChart,
  VictoryAxis,
  VictoryCandlestick,
  VictoryLabel,
  VictoryTooltip,
  VictoryVoronoiContainer,
  Flyout
} from 'victory';
import './Candlechart.css';

export default props => (
  <div className="candle-chart">
    <VictoryChart
      width={1450}
      height={600}
      domain={{y:[props.periodLow - (props.periodHigh - props.periodLow)*0.1, props.periodHigh + (props.periodHigh - props.periodLow)*0.1]}}
      containerComponent={
        <VictoryVoronoiContainer
          voronoiDimension="x"
        />
      }
    >
      <VictoryAxis
        orientation="left"
        style={{
          axis: {display: "none"},
          grid: {
            stroke: "#384150"
          }
        }}
        tickLabelComponent={<VictoryLabel style={{fill:"#ffffff", fontSize: 22}}/>}
        offsetX={30}
      />
      <VictoryAxis
        dependentAxis
        orientation='bottom'
        tickLabelComponent={<VictoryLabel style={{fill:"#ffffff", fontSize: 22}}/>}
        tickFormat={t=>{
          return props.period > 600 ?
            moment(parseInt(t+'000', 10)).format('MMMM Do, H:mm:ss') :
            moment(parseInt(t+'000', 10)).format('HH:mm:ss')
        }}
      />
      <VictoryCandlestick
        data={props.periodData}
        candleColors={{ positive: "#05bcfe", negative: "transparent" }}
        style={{
          data: {
            stroke: '#05bcfe'
          }
        }}
        x="time"
        labels={(d) =>
          `Time: ${moment(parseInt(d.time+'000', 10)).format('YYYY MMMM Do, H:mm:ss')}
                          Open: ${d.open}
                          Close: ${d.close}
                          High: ${d.high}
                          Low: ${d.low}`
        }
        labelComponent={<VictoryTooltip
          style={{fontSize: 22}}
          flyoutComponent={
            <Flyout
              width={320}
            />
          }
          flyoutStyle={{
            fill: 'rgba(5, 188, 254, 0.75)'
          }}
        />}
      />
    </VictoryChart>
  </div>
)