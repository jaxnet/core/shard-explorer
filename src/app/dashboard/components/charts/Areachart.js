import React from 'react';
import {
  VictoryChart,
  VictoryAxis,
  VictoryArea,
  VictoryLabel,
  VictoryVoronoiContainer,
  Flyout,
  VictoryTooltip
} from 'victory';
import './Areachart.css';
import moment from "moment/moment";

export default props => (
  <div className="area-chart">
    <VictoryChart
      width={1450}
      height={600}
      domainPadding={[25,25]}
      domain={
        {
          y:[props.momentLow - (props.momentHigh - props.momentLow)*0.1, props.momentHigh + (props.momentHigh - props.momentLow)*0.1]
        }
      }
      containerComponent={
        <VictoryVoronoiContainer/>
      }
    >
      <VictoryAxis
        orientation="left"
        style={{
          axis: {display: "none"},
          grid: {
            stroke: "#384150"
          }
        }}
        tickLabelComponent={<VictoryLabel style={{fill:"#ffffff", fontSize: 22}}/>}
      />
      <VictoryArea
        data={props.momentData}
        style={{
          data: {fill: "rgba(10, 218, 254, 0.3)", stroke: "#05bcfe", strokeWidth: 3},
          labels: {fontSize: 18, fill: "#fff"}
        }}
        interpolation='natural'
        x="time"
        y="price"
        labels={(d) =>
          `Time: ${moment(parseInt(d.time, 10)).format('YYYY MMMM Do, H:mm:ss')}
                          Price: ${d.price}`
        }
        labelComponent={<VictoryTooltip
          style={{fontSize: 22}}
          flyoutComponent={
            <Flyout
              width={320}
            />
          }
          flyoutStyle={{
            fill: 'rgba(5, 188, 254, 0.75)'
          }}
        />}
      />
    </VictoryChart>
  </div>
);