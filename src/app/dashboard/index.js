import React from 'react';
import {bindActionCreators, compose} from 'redux';
import {connect} from 'react-redux';
import {withRouter,} from 'react-router-dom';
import './index.scss'
import {InnerLayout} from "../../layout";

class Dashboard extends React.Component{
    render(){
        return (
            <InnerLayout>
                <section className="dashboard">
                </section>
            </InnerLayout>
        );
    }
}

const mapStateToProps = state => ({
    charts: state.charts
});
const mapDispatchToProps = dispatch => bindActionCreators({
}, dispatch);

export default compose(
    withRouter,
    connect(mapStateToProps, mapDispatchToProps)
)(Dashboard);
