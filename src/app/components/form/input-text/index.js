import React, { Component } from "react";
import "./input-text.css"
class InputText extends Component {
  render() {
    const { onChange, placeholder, label, value, name, isInline} = this.props;
    return (
      <div className={`input input__text ${isInline ? "input__text--row": "input__text--column"}`}>
        {label && <label htmlFor={name}>{label}</label>}
        <input
          className="form-control bg-transparent text-light"
          placeholder={placeholder}
          type="text"
          value={value}
          name={name}
          id={name}
          onChange={onChange}
        />
      </div>
    );
  }
}
export default InputText;
