import React, { Component } from "react";
import "./filter-btn.css";
class FilterBtn extends Component {
  render() {
    const { name, label, onClick, checked } = this.props;
    return (
      <div
        className={`filter-btn${checked ? " filter-btn--active" : ""}`}
        onClick={() => onClick(name)}
      >
        {label}
      </div>
    );
  }
}

export default FilterBtn;
