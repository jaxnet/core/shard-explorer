import React from "react";


const page = (shift, current, take, pages, loadPage) => {
    let page = current + shift
    return page >= 0 && page < pages && (
        <li className="page-item">
            <button className="btn btn-sm page-link"
               onClick={() => loadPage(page * take, take)}>{page + 1}</button>
        </li>
    )
};

export const Pager = (skip, take, total, loadPage) => {
    skip = skip || 0
    let pagesCount = Math.ceil((total) / take);
    let currentPage = Math.floor(skip / take);
    let pages = {}
    for (let i = 0; i < pagesCount; i++) {
        pages[i] = {index: i, caption: i + 1}
    }
    currentPage = Math.max(currentPage, 0);
    currentPage = Math.min(pagesCount - 1, currentPage)

    return pagesCount > 0 && (
        <nav aria-label="Page navigation example">
            <ul className="pagination">
                <li className={"page-item " + (currentPage === 0 ? 'disabled' : '')}>
                    <button onClick={() => loadPage(0, take)} className="btn btn-sm page-link" href="#"
                       tabIndex="-2">&lsaquo;&lsaquo;</button>
                </li>
                <li className={"page-item " + (currentPage === 0 ? 'disabled' : '')}>
                    <button onClick={() => loadPage((currentPage - 1) * take, take)} className="btn btn-sm page-link" href="#"
                       tabIndex="-1">Prev</button>
                </li>
                {page(-2, currentPage, take, pagesCount, loadPage)}
                {page(-1, currentPage, take, pagesCount, loadPage)}
                {(<li className="page-item active">
                    <button className="page-link"
                       onClick={() => loadPage(currentPage * take, take)}>{pages[currentPage].caption}</button>
                </li>)}
                {page(1, currentPage, take, pagesCount, loadPage)}
                {page(2, currentPage, take, pagesCount, loadPage)}
                <li className={"page-item " + (currentPage === pagesCount - 1 ? 'disabled' : '')}>
                    <button onClick={() => loadPage((currentPage + 1) * take, take)}
                       className="btn btn-sm page-link" href="#" tabIndex="+1">Next</button>
                </li>
                <li className={"page-item " + (currentPage === pagesCount - 1 ? 'disabled' : '')}>
                    <button onClick={() => loadPage((pagesCount - 1) * take, take)}
                       className="btn btn-sm page-link" href="#" tabIndex="+1">&rsaquo;&rsaquo;</button>
                </li>
            </ul>
        </nav>
    )
}