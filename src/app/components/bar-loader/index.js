import React, { Component } from "react";
import "./index.scss";
class BarLoader extends Component {
  render() {
    const {text, percentage} = this.props;
    return (
      <div className="bar-loader">
        <div className="bar-loader__text">{text}</div>
        <div className="bar-loader__bar">
          <div className="bar-loader__bar__percentage" style={{width: `${percentage}%`}}/>
        </div>
      </div>
    );
  }
}
export default BarLoader;