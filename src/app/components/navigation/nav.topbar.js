import React from 'react';
import {IoIosContact as ProfileIcon} from 'react-icons/io';
import {FaBars as Bars} from 'react-icons/fa/';

import './index.scss'
import MainMenu from "./menu.main";

export class TopBar extends React.Component {
    constructor() {
        super();
        this.state = {
            showNotifications: false,
        }
    }

    render() {
        // const {toggleSidebar} = this.props;
        return (
            <header className="top-bar">
                <div>
                    <div className="logo" onClick={() => this.props.navigate("/")}><span>Jax</span>Explorer</div>
                    <div className="nav nav-header nav-main">
                        <MainMenu/>
                    </div>
                </div>
                <div className="burger" onClick={() => this.props.toggleLeft()}><Bars className="burger-wrapper"/>
                </div>
            </header>
        );
    }
}
