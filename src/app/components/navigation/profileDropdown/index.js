import React from 'react';
import onClickOutside from "react-onclickoutside";
import { IoIosContact as ProfileIcon} from 'react-icons/io';
import {IoIosPower as ExitIcon} from 'react-icons/io/';
import './index.scss'
import {withRouter, Link} from "react-router-dom";
import {connect} from "react-redux";
import {bindActionCreators, compose} from "redux";

class profileDropdown extends React.Component {
  handleClickOutside = () => {
    // this.props.toggleProfile();
  };
  render() {
    return (
      <div className="profile-dropdown">
        <div className="header">
          {/*<img className="avatar" src={avatar} alt=""/>*/}
          {/*<div className="wrapper-name">*/}
          {/*  <div className="name">{this.props.user.name}</div>*/}
          {/*  <div className="email">{this.props.user.role.admin ? 'Admin' : 'User'}</div>*/}
          {/*</div>*/}
        </div>
        <div className="wrapper">
          <nav className="nav flex-column">
            <Link to="/account" className="nav-link d-flex justify-content-between">
              <div className="col pl-0">
                <ProfileIcon className="mr-2"/>
                My profile
              </div>
            </Link>
            <div onClick={this.props.logout} className="nav-link d-flex justify-content-between">
              <div className="col pl-0">
                <ExitIcon className="mr-2"/>
                Logout
              </div>
            </div>
          </nav>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  // user: state.auth.user
});
const mapDispatchToProps = dispatch => bindActionCreators({
  // logout: Auth.Logout
}, dispatch);

export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps)
)(onClickOutside(profileDropdown));