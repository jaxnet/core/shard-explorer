import React from 'react';
import {FaArrowLeft as ArrowLeft} from 'react-icons/fa/';
import MainMenu from "./menu.main";
import onClickOutside from "react-onclickoutside";

class LeftBarClass extends React.Component {
    constructor() {
        super();
        this.state = {
            showNotifications: false,
            showProfile: false
        }
    }

    handleClickOutside = (e) => {
        if (e.clientY > 50) {
            this.props.closeBar();
        }
    };

    render() {
        const {isCollapsed, closeBar} = this.props;
        return (
            <nav className={isCollapsed ? 'left-bar collapsed' : 'left-bar'}>
                <div className="dismiss" onClick={() => closeBar()}>
                    <ArrowLeft/>
                </div>
                <div className="sidebar-header">
                    <h3>Menu</h3>
                </div>
                <MainMenu closeBar={closeBar}/>
            </nav>
        );
    }
}

export const LeftBar = onClickOutside(LeftBarClass);