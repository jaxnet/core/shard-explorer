import React from 'react';
import {bindActionCreators, compose} from 'redux';
import {connect} from 'react-redux';

import './index.scss'
import {TopBar} from "./nav.topbar";
import {LeftBar} from "./nav.leftbar";
import {System} from "../../../modules/module.system";
import {push} from "connected-react-router";


class Navigation extends React.Component {
    render() {
        const {closeLeft, closeRight, openLeft, openRight, left, right, navigate} = this.props;
        return (
            <React.Fragment>
                <TopBar toggleLeft={() => left ? closeLeft() : openLeft()}
                        toggleRight={() => right ? closeRight() : openRight()} navigate={navigate}/>
                <LeftBar isCollapsed={!left} closeBar={closeLeft}/>
            </React.Fragment>
        );
    }
}

const mapStateToProps = ({system}) => ({
    left: system.getIn(['ui', 'left', 'show']),
});

const mapDispatchToProps = dispatch => bindActionCreators(
    {
        closeLeft: System.UI.Left.Close,
        closeRight: System.UI.Right.Close,
        openLeft: System.UI.Left.Show,
        navigate: path => push(path)
    },
    dispatch);

export default compose(
    connect(mapStateToProps, mapDispatchToProps)
)(Navigation);
