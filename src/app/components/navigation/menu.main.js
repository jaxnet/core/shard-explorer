import React, {Component} from "react";

import "./index.scss"
// import {MdDashboard as HomeIcon} from "react-icons/md";
// import {
//     IoIosApps as AppsIcon,
//     IoMdSettings as SettingsIcon,
//     IoMdStats as StatIcons,
// } from "react-icons/io";

import { NavLink} from 'react-router-dom'
import {connect} from "react-redux";

class MainMenuClass extends Component {
    render() {
        return (
            <ul className="navbar-nav" onClick={this.props.closeBar}>
                <NavLink exact to="/" className="nav-item">
                    <span className="text">Dashboard</span>
                </NavLink>
                <NavLink to="/explorer" className="nav-item">
                    <span className="text">Explorer</span>
                </NavLink>

                <NavLink to="/stat" className="nav-item">
                    <span className="text">Statistics</span>
                </NavLink>

                {/*<NavLink to="/settings" className="nav-item">*/}
                {/*    <span className="text">Settings</span>*/}
                {/*</NavLink>*/}
            </ul>
        )
    }
}

export default connect(null, {})(MainMenuClass);