import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import {lighten} from '@material-ui/core/styles/colorManipulator';


const toolbarStyles = theme => ({
    root: {
        paddingRight: theme.spacing.unit,
    },
    highlight:
        theme.palette.type === 'light'
            ? {
                color: theme.palette.secondary.main,
                backgroundColor: lighten(theme.palette.secondary.light, 0.85),
            }
            : {
                color: theme.palette.text.primary,
                backgroundColor: theme.palette.secondary.dark,
            },
    spacer: {
        flex: '1 1 100%',
    },
    actions: {
        color: theme.palette.text.secondary,
        flex: '0 0 auto',
    },
    title: {
        flex: '0 0 auto',
    },
});

let TableToolbar = props => {
    const {selected, classes, toolbar} = props;
    const anySelected = selected.length > 0;
    let display = (v, i) => (
        <Tooltip title={v.title} key={i} >
            <IconButton aria-label={v.title} onClick={() => v.action(selected)}>
                {v.icon}
            </IconButton>
        </Tooltip>)
    return (
        <Toolbar className={anySelected ? classes.root + " " + classes.highlight : classes.root}>
            <div className={classes.title}>
                {anySelected ? (
                    <Typography color="inherit" variant="subtitle1">
                        {selected.length} selected
                    </Typography>
                ) : (
                    <Typography variant="h6" id="tableTitle">
                        {toolbar.Label}
                    </Typography>
                )}
            </div>
            <div className={classes.spacer}/>
            <div className={classes.actions}>
                {toolbar.Actions.map((v, i) => {
                    switch (v.type) {
                        case "any":
                            return display(v, i);
                        case "selected":
                            if (anySelected) {
                                return display(v, i);
                            }
                            break;
                        default:
                            return ("")
                    }
                    return ("")
                })}

            </div>
        </Toolbar>
    );
};

export default withStyles(toolbarStyles)(TableToolbar);

// {numSelected > 0 ? (
//     <Tooltip title="Delete">
//         <IconButton aria-label="Delete">
//             <DeleteIcon/>
//         </IconButton>
//     </Tooltip>
// ) : (
//     <Tooltip title="Filter list">
//         <IconButton aria-label="Filter list">
//             <FilterListIcon/>
//         </IconButton>
//     </Tooltip>
// )}