import React from 'react';

import {withStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import TableHeader from "./table.header";
import TableToolbar from './table.toolbar'


const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 1,
    },
    table: {
        minWidth: 300,
    },
    tableWrapper: {
        overflowX: 'auto',
    },
});

class TableComponent extends React.Component {
    state = {
        order: 'asc',
        orderBy: 'calories',
        selected: [],
    };
    componentDidMount = () => {
        this.reload(this.props.data.filter)
    };

    reload = (filter) => {
        this.props.onUpdate(filter)
    }

    handleRequestSort = (event, property) => {
        const orderBy = property;
        let order = 'desc';

        if (this.state.orderBy === property && this.state.order === 'desc') {
            order = 'asc';
        }

        this.setState({order, orderBy});
    };

    handleSelectAllClick = event => {
        if (event.target.checked) {
            this.setState(() => ({selected: this.props.data.list.map(n => n.ID)}));
            return;
        }
        this.setState({selected: []});
    };

    handleClick = (event, id) => {
        const {selected} = this.state;
        const selectedIndex = selected.indexOf(id);
        let newSelected = [];

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected, id);
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1));
        } else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                selected.slice(0, selectedIndex),
                selected.slice(selectedIndex + 1),
            );
        }

        this.setState({selected: newSelected});
    };

    handleChangePage = (event, page) => {
        let filter = this.props.data.filter;
        filter.skip = page * filter.take;
        filter.page = page;
        this.reload(filter)
    };

    handleChangeRowsPerPage = event => {
        let filter = this.props.data.filter;
        filter.take =  event.target.value;
        this.reload(filter)
    };

    isSelected = id => this.state.selected.indexOf(id) !== -1;

    render() {
        const {classes, toolbar, columns, data} = this.props;
        const items = data.list;
        const total = data.total;
        const filter = data.filter;
        const {order, orderBy, selected} = this.state;

        return (
            <Paper className={classes.root}>
                <TableToolbar toolbar={toolbar} selected={selected}/>
                <div className={classes.tableWrapper}>
                    <Table className={classes.table} aria-labelledby="tableTitle">
                        <TableHeader
                            columns={columns}
                            numSelected={selected.length}
                            order={order}
                            orderBy={orderBy}
                            onSelectAllClick={this.handleSelectAllClick}
                            onRequestSort={this.handleRequestSort}
                            rowCount={items.length}
                        />
                        <TableBody>
                            {items.map(n => {
                                const isSelected = this.isSelected(n.ID);
                                return (
                                    <TableRow
                                        hover
                                        onClick={event => this.handleClick(event, n.ID)}
                                        role="checkbox"
                                        aria-checked={isSelected}
                                        tabIndex={-1}
                                        key={n.ID}
                                        selected={isSelected}>
                                        <TableCell padding="checkbox">
                                            <Checkbox checked={isSelected}/>
                                        </TableCell>

                                        {columns && columns.map((v, i) => (
                                            <TableCell align="left" key={i}>{n[v.id]}</TableCell>
                                        ))}
                                    </TableRow>
                                );
                            })}
                        </TableBody>
                    </Table>
                </div>
                <TablePagination
                    rowsPerPageOptions={[5, 10, 25]}
                    component="div"
                    count={total}
                    rowsPerPage={filter.take}
                    page={filter.page}
                    backIconButtonProps={{
                        'aria-label': 'Previous Page',
                    }}
                    nextIconButtonProps={{
                        'aria-label': 'Next Page',
                    }}
                    onChangePage={this.handleChangePage}
                    onChangeRowsPerPage={this.handleChangeRowsPerPage}
                />
            </Paper>
        );
    }
}

export default withStyles(styles)(TableComponent);
