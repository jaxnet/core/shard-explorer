import React from 'react';
import './index.scss';

export const Preloader = (props) => <div className={`preloader ${props.small ? `small` : ``}`}><div className="preloader-inner"/>Loading, please wait</div>;