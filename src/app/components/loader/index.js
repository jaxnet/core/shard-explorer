import React from 'react';
import './index.scss'
import {bindActionCreators, compose} from "redux";
import {withRouter} from "react-router-dom";
import connect from "react-redux/es/connect/connect";

class Loader extends React.Component {
    render() {
        return (
            <React.Fragment>
                {!this.props.hide && (
                    <div className="background-loader container-loader">
                        <div className="triangles">
                            <div className="tri invert"/>
                            <div className="tri invert"/>
                            <div className="tri"/>
                            <div className="tri invert"/>
                            <div className="tri invert"/>
                            <div className="tri"/>
                            <div className="tri invert"/>
                            <div className="tri"/>
                            <div className="tri invert"/>
                        </div>
                    </div>
                )}
            </React.Fragment>
        )
    }
}


const mapStateToProps = ({system}) => ({
    hide: system.getIn(['ui', 'lock']).isEmpty()
});

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default compose(
    withRouter,
    connect(mapStateToProps, mapDispatchToProps)
)(Loader);
