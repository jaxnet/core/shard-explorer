import React from 'react';
import {bindActionCreators, compose} from 'redux';
import {connect} from 'react-redux';
import {withRouter,} from 'react-router-dom';

class Loading extends React.Component {
    componentDidMount = () => {
    };

    render() {
        return (
            <div>
                <h1>Loading</h1>
            </div>
        );
    }
}

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default compose(
    withRouter,
    connect(mapStateToProps, mapDispatchToProps)
)(Loading);
