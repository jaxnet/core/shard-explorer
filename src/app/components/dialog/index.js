import React from 'react';
import './index.scss';


export const Dialog = ({show, close, children}) => <React.Fragment>
    <div className={show ? 'modal-container' : 'modal-container collapsed'}>
        <div className="dialog d-flex flex-column">
            <div className="p-2 d-flex flex-row actions">
                <button type="button" className="btn btn-secondary" onClick={close}>Close</button>
            </div>
            {children}
        </div>
    </div>
</React.Fragment>;