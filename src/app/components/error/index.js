import React from 'react';

export const Error = ({message}) => <div className="alert alert-info">{message}</div>;