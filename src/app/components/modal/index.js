import React from 'react';
import {bindActionCreators, compose} from 'redux';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';

import './index.scss'
import {System} from "../../../modules/module.system";
import {push} from "connected-react-router";
import {Field, reduxForm} from 'redux-form';

const textElement = ({label, name}) => (
    <React.Fragment>
        <label htmlFor={name}>{label}</label>
        <Field type="text" component="input" className="form-control" name={name} label={label}/>
    </React.Fragment>
)

const selectElement = ({label, name, value, values, display}) => (
    <React.Fragment>
        <label htmlFor={name}>{label}</label>
        <Field className="form-control" name={name} label={label} component="select">
            <option/>
            {values.map((v, i) => (
                <option value={display.key(v)} key={i}>{display.label(v)}</option>
            ))}
        </Field>
    </React.Fragment>
)

const formElement = (item) => {
    if (!item) {
        return
    }
    switch (item.type) {

        case "select":
            return selectElement(item);
        default:
            return textElement(item);
    }
};

const formActions = (item, onConfirm, onCancel) => {
    if (!item) {
        return
    }
    switch (item.type) {
        case "confirm":
            return <button type="button" className="btn btn-primary"
                           onClick={() => onConfirm(item.action)}>{item.label}</button>
        case "cancel":
            return <button type="button" className="btn btn-secondary" onClick={() => onCancel()}>{item.label}</button>
        default:
            return ""
    }
};

class ModalDialog extends React.Component {
    render() {
        const {modal, confirm, state, close} = this.props;
        return (
            <React.Fragment>
                <div className={modal.show ? 'modal-container' : 'modal-container collapsed'}>
                    <div className="dialog d-flex flex-column">
                        <div className="p-2">
                            {modal.header &&
                            <React.Fragment>
                                <h3>{modal.header.Title}</h3>
                                <p>{modal.header.Description}</p>
                            </React.Fragment>
                            }
                        </div>
                        <div className="p-2">

                            {modal.form && modal.form.map((v, i) =>
                                <div className="form-group" key={i}>
                                    {formElement(v)}
                                </div>
                            )}

                        </div>
                        <div className="p-2 d-flex flex-row actions">
                            {modal.actions && modal.actions.map((v, i) =>
                                <div key={i}>
                                    {formActions(v, (action) => {
                                        action.payload = Object.assign({}, action.payload, state.values)
                                        confirm(action);
                                        close()
                                    }, close)}
                                </div>
                            )}
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = ({system, form}) => ({
    modal: system.get('modal').toJS(),
    state: form.dialog
});

const mapDispatchToProps = dispatch => bindActionCreators(
    {
        confirm: (action) => action,
        close: System.Modal.Close,
        navigate: path => push(path)
    },
    dispatch);

export default compose(
    withRouter,
    connect(mapStateToProps, mapDispatchToProps)
)(reduxForm({
    form: 'dialog'
})(ModalDialog));


