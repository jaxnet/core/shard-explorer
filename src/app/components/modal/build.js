const action = (dialog) => ({
    Confirm: (label, f) => {
        dialog.actions.push({type: 'confirm', label, action: f});
        return action(dialog)
    },
    Cancel: (label) => {
        dialog.actions.push({type: 'cancel', label});
        return action(dialog)
    },
    Done: () => {
        return dialog
    }
});

// const display = {
//     ID: (v) => v
// }

const form = (dialog) => ({
    Text: (name, label, value) => {
        dialog.form.push({type: 'text', name, label, value});
        if (value) {
            dialog.values[name] = value;
        }
        return form(dialog)
    },
    Number: (name, label, value) => {
        dialog.form.push({type: 'number', name, label, value});
        if (value) {
            dialog.values[name] = value;
        }
        return form(dialog)
    },
    Slider: (name, label, value, from, to) => {
        dialog.form.push({type: 'slider', name, label, from, to});
        if (value) {
            dialog.values[name] = value;
        }
        return form(dialog)
    },
    Select: (name, label, value, values, config) => {
        const display = config ? config : ({key: (v) => v, label: (v) => v});
        dialog.form.push({type: 'select', name, label, value, values, display});
        if (value) {
            dialog.values[name] = value;
        }
        return form(dialog)
    },
    Action: () => {
        return action(dialog)
    },
    Done: () => {
        return dialog
    }
});


export const Dialog = (header, description, values = {}) => {
    let dialog = {header: {Title: header, Description: description}, form: [], actions: [], values: values};
    return form(dialog)
};